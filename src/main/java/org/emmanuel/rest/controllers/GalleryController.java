package org.emmanuel.rest.controllers;

import java.util.List;

import org.emmanuel.rest.models.Gallery;
import org.emmanuel.rest.repositories.GalleryRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gallery")
public class GalleryController {
	private final GalleryRepository repository;
	
	public GalleryController(GalleryRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping(path="/get/{galleryId:\\d+}")
	public Gallery getGallery(@PathVariable(name="galleryId") Long galleryId) {
		return repository.findById(galleryId).get();
	}
	
	@GetMapping(path="/getAll/{groupId:\\d+}")
	public List<Gallery> getGalleries(@PathVariable(name="groupId") Integer groupId) {
		return repository.getGalleriesByGroupNumber(groupId);
	}
}
