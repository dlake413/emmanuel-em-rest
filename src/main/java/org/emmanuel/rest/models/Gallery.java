package org.emmanuel.rest.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="gallery")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Gallery {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="imgUrl")
	private String imgUrl;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description", columnDefinition="text")
	private String description;
	
	@Column(name="link")
	private String link;
	
	@Column(name="activeflg")
	private Boolean activeflg;
	
	@Column(name="groupNumber")
	private Integer groupNumber;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Boolean getActiveflg() {
		return activeflg;
	}
	public void setActiveflg(Boolean activeflg) {
		this.activeflg = activeflg;
	}
	public Integer getGroupNumber() {
		return groupNumber;
	}
	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}
	
}
