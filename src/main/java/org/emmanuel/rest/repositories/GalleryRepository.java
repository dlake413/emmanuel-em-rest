package org.emmanuel.rest.repositories;

import java.util.List;

import org.emmanuel.rest.models.Gallery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GalleryRepository extends CrudRepository<Gallery, Long> {
	@Query(value="SELECT g FROM Gallery g WHERE groupNumber = :groupNumber")
	public List<Gallery> getGalleriesByGroupNumber(@Param("groupNumber") Integer groupNumber);
}
